require 'rails_helper'

RSpec.describe Section, type: :model do
  describe 'attributes' do
    it 'can access and read name'

    it 'can access and read position'

    it 'can access and read visibility'

    it 'can access and view content_type'

    it 'can access and view content'
  end

  describe 'associations' do
    it 'belongs with a page' do

    end
  end

  describe 'scopes' do

    it 'has a scope to see visible sections'
  end
end
