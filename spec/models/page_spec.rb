require 'rails_helper'

RSpec.describe Page, type: :model do
  describe 'attributes' do
    it 'can access and read name'

    it 'can access and read permalink'

    it 'can access and read position'

    it 'can access and read visibility'
  end

  describe 'associations' do
    it 'belongs to a subject'
  end
end
