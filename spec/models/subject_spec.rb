require 'rails_helper'

RSpec.describe Subject, type: :model do
  describe 'attributes' do
    it 'can access and set name' do

    end

    it 'can access and set position' do

    end

    it 'can access and set visibility' do

    end
  end

  describe 'validations' do

    context '#name' do
      it 'must have a name' do
        subject = Subject.new(:visible => true, :position => 0)
        expect(subject.valid?).to be false
      end

      it 'name succeeds' do
        subject = Subject.new(:visible => true, :position => 0, :name => 'Foo Bar')
        expect(subject.valid?).to be true
      end
    end

    context '#visible' do
      it 'must have a visible set' do
        subject = Subject.new(:name => 'Subject Test', :position => 0, :visible => true)
        expect(subject.valid?).to be true
      end
    end

    context '#position' do
      it 'must have a position set' do
        subject = Subject.new(:name => 'Subject test', :visible => true)
        expect(subject.valid?).to be false
      end

      it 'must be greater than 0' do
        subject = Subject.new(:name => 'Subject test', :visible => true, :position => -1)
        expect(subject.valid?).to be false
        expect(subject.errors[:position].first).to include("must be greater than or equal to 0")
      end
    end
  end

  describe 'scopes' do

    context '.visible' do
      it 'has a visible scope' do
        expect { Subject.visible }.to_not raise_error(NoMethodError)
      end

      it 'scope returns all visible subjects' do
        subj1 = FactoryGirl.create(:subject, :visible => true)
        subj2 = FactoryGirl.create(:subject, :visible => false)

        expect(Subject.visible.size).to eq(1)
        expect(Subject.visible.first).to eq(subj1)
      end
    end

    context '.sorted' do
      it 'has a sorted scope' do
        expect { Subject.sorted }.to_not raise_error(NoMethodError)
      end

      it 'scope returns all subjects in postion ASC order' do
        s1 = FactoryGirl.create(:subject, :position => 1)
        s2 = FactoryGirl.create(:subject, :position => 3)
        s3 = FactoryGirl.create(:subject, :position => 2)

        positions = Subject.sorted.map { |subj| subj.position }
        expect(positions).to eq([1,2,3])
      end
    end

    context '.search' do
      it 'has a search scope' do
        expect { Subject.search }.to_not raise_error(NoMethodError)
      end

      it 'allows for optional parameter' do
        expect { Subject.search('Baby Formula') }.to_not raise_error(ArgumentError)
      end

      it 'scope is on the name field' do
        expect(Subject.search('Baby Formula').to_sql).to include('name RLIKE')
      end

      it 'returns the subjects that almost match the name' do
        subj1 = FactoryGirl.create(:subject, :name => 'Childhood growth')
        subj2 = FactoryGirl.create(:subject, :name => 'Children Playground')

        expect(Subject.search('hild')).to include(subj1, subj2)
      end

      it 'returns empty list for none match' do
        subj1 = FactoryGirl.create(:subject, :name => 'Childhood growth')
        expect(Subject.search('Car')).to eq([])
      end
    end
  end

end
