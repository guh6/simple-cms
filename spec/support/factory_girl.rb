RSpec.configure do |config|
  config.include FactoryGirl::Syntax::Methods
end


FactoryGirl.define do
  factory :page do

  end

  factory :section do
    
  end

  factory :subject do
    name "Random Subject"
    position 0
    visible false
  end
end
