require 'rails_helper'

RSpec.describe SubjectsController, type: :controller do

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end

    it 'uses the sorted scope' do
      expect(Subject).to receive(:sorted)
      get :index
    end

    it 'returns a list of subjects in sorted order' do
      subj1 = FactoryGirl.create(:subject, :position => 5)
      subj2 = FactoryGirl.create(:subject, :position => 3)

      get :index
      subjects = controller.instance_variable_get(:@subjects)
      expect(subjects).to eq([subj2, subj1])
    end
  end

  describe "GET #show" do
    let(:subject){ FactoryGirl.create(:subject) }

    it "returns http success" do
      get :show, :params => { :id => subject.id }
      expect(response).to have_http_status(:success)
    end

    it 'returns not found if the record is not found' do
      get :show, :params => { :id => (subject.id + 1) }
      expect(response.body).to include ("Record not found")
    end

    it 'returns the show view if found' do
      get :show, :params => { :id => subject.id }
      expect(response).to render_template(:show)
    end

    it 'sets the subject ivar if found' do
      get :show, :params => { :id => subject.id }
      expect(controller.instance_variable_get(:@subject)).to eq(subject)
    end
  end

  describe "GET #new" do
    it "returns http success" do
      get :new
      expect(response).to have_http_status(:success)
    end

    it 'renders the new page' do
      get :new
      expect(response).to render_template(:new)
    end

    it 'creates a new subject object to use' do
      get :new
      expect(controller.instance_variable_get(:@subject)).to_not eq(nil)
    end

    it 'should default visible to true' do
      get :new
      expect(controller.instance_variable_get(:@subject).visible).to be_truthy
    end
  end

  describe "POST #create" do
    let(:subject){
      {
        :name => 'Bicycle Repair',
        :visible => true,
        :position => 4
      }
    }

    let(:whitelist){ double("whitelist") }

    it 'uses the params to create the object' do
      post :create, :params => { :subject => subject }
      expect(controller.instance_variable_get(:@subject)).to_not be_nil
    end

    it 'uses whitelisting on the params' do
      allow(controller).to receive(:create_params).and_return(whitelist)
      allow(Subject).to receive(:create).with(whitelist).and_return(true)
      expect(Subject).to receive(:create).with(whitelist)
      post :create, :params => { :subject => subject }
    end

    it 'redirects to index on success' do
      subj = instance_double('Subject', :save => true)
      allow(Subject).to receive(:create).and_return(subj)
      post :create, :params => { :subject => subject }
      expect(response).to redirect_to(subjects_path)
    end

    it 'renders page on failure' do
      subj = instance_double('Subject', :save => false)
      allow(Subject).to receive(:create).and_return(subj)
      post :create, :params => { :subject => subject }
      expect(response).to render_template(:new)
    end

    it 'sets error in flash' do
      subj = instance_double('Subject', :save => false, :errors =>{:attr => ['Something went wrong']})
      allow(Subject).to receive(:create).and_return(subj)
      post :create, :params => { :subject => subject }
      expect(flash[:error]).to include(:attr => ["Something went wrong"])
    end

    it 'has the correct attributes' do
      post :create, :params => {:subject => subject}
      expect(controller.instance_variable_get(:@subject)).to include(subject)
    end
  end

  describe 'GET #edit' do
    let(:subject){ FactoryGirl.create(:subject, :name => 'I am subject',
                                                :position => 1,
                                                :visible => false) }


    it 'returns http success' do
      get :edit, :params => { :id =>  subject.id}
      expect(response).to have_http_status(:success)
    end

    it 'returns subject not found for invalid subject' do
      allow(Subject).to receive(:find).with(subject.id.to_s){ raise ActiveRecord::RecordNotFound.new }
      get :edit, :params => { :id =>  subject.id}
      expect(response.body).to include ('Subject not found!')
    end

    it 'renders the edit page on successful retrieval' do
      get :edit, :params => { :id =>  subject.id}
      expect(response).to render_template(:edit)
    end
  end

  describe 'PATCH #update' do
    let(:subject){ FactoryGirl.create(:subject) }
    let(:changes){
      {
        :name => 'New name',
        :visible => false,
        :position => 99
      }
    }

    it 'updates with strong parameters' do
      whitelist = double('whitelist')
      allow(Subject).to receive(:find).and_return(subject)
      allow(controller).to receive(:create_params).and_return(whitelist)
      allow(subject).to receive(:update_attributes).with(whitelist).and_return(true)
      expect(subject).to receive(:update_attributes).with(whitelist)

      patch :update, :params => {:id => subject.id, :subject => changes }
    end

    it 'on failure, render edit page' do
      allow(Subject).to receive(:find).and_return(subject)
      allow(subject).to receive(:update_attributes).and_return(false)
      patch :update, :params => {:id => subject.id, :subject => changes }

      expect(response).to render_template(:edit)
    end

    it 'on failure, sets the error message' do
      allow(subject).to receive(:errors).and_return("errors")
      allow(Subject).to receive(:find).and_return(subject)
      allow(subject).to receive(:update_attributes).and_return(false)
      patch :update, :params => {:id => subject.id, :subject => changes }
      expect(flash[:errors]).to include("errors")
    end

    it 'on no record found, redirect to index' do
      allow(Subject).to receive(:find) { raise ActiveRecord::RecordNotFound }
      patch :update, :params => {:id => subject.id, :subject => changes }

      expect(response).to redirect_to(subjects_path)
    end

    it 'on no record found, set flash' do
      allow(Subject).to receive(:find) { raise ActiveRecord::RecordNotFound }
      patch :update, :params => {:id => subject.id, :subject => changes }

      expect(flash[:errors]).to include('Not found')
    end

    it 'on success, redirect to show page' do
      allow(subject).to receive(:update_attributes).and_return(true)
      allow(Subject).to receive(:find).and_return(subject)
      patch :update, :params => {:id => subject.id, :subject => changes }

      expect(response).to redirect_to(subject_path(subject))
    end

    it 'on error, redirect to show page with flash' do
      allow(Subject).to receive(:find) { raise Exception::RunTimeError}
      patch :update, :params => {:id => subject.id, :subject => changes }

      expect(flash[:errors]).to include('PROBLEM WITH UPDATE')
      expect(response).to redirect_to(subject_path(subject))
    end
  end

  describe 'GET #delete' do
    it 'returns http success on object found' do
      allow(Subject).to receive(:find).and_return(true)

      get :delete, :params => {:id => 1}
      expect(response).to have_http_status(:success)
    end

    it 'redirect to index if record not found' do
      allow(Subject).to receive(:find){ raise ActiveRecord::RecordNotFound}
      get :delete, :params => {:id => 1}
      expect(response).to redirect_to(subjects_path)
    end

    it 'sets the flash to indicate record not found' do
      allow(Subject).to receive(:find){ raise ActiveRecord::RecordNotFound}
      get :delete, :params => {:id => 1}
      expect(flash[:errors]).to include("Record Not Found")
    end

    describe 'DELETE #destroy' do
      before(:each){ @subject = FactoryGirl.create(:subject) }

      it 'redirect to index page after delete' do
        delete :destroy, :params => {:id => @subject.id}
        expect(response).to redirect_to(subjects_path)
      end

      it 'renders delete page for error delete' do
        allow(@subject).to receive(:destroy).and_return(false)
        allow(Subject).to receive(:find).and_return(@subject)
        delete :destroy, :params => {:id => @subject.id}
        expect(response).to render_template(:delete)
      end

      it 'sets flash error for validation failures' do
        allow(@subject).to receive_messages(:destroy => false, :errors => 'Some errors')
        allow(Subject).to receive(:find).and_return(@subject)
        delete :destroy, :params => {:id => @subject.id}
        expect(flash[:errors]).to include('Some errors')
      end

      it 'renders delete page for general exception error' do
        allow(Subject).to receive(:find){ raise RunTimeError}
        delete :destroy, :params => {:id => @subject.id}
        expect(response).to render_template(:delete)
      end

      it 'sets application error' do
        allow(Subject).to receive(:find){ raise Exception::RuntimeError.new('App error') }
        delete :destroy, :params => {:id => @subject.id}
        expect(flash[:errors]).to include('App error')
      end

      it 'redirects to index for record not found' do
        allow(Subject).to receive(:find){ raise ActiveRecord::RecordNotFound}
        delete :destroy, :params => {:id => @subject.id}
        expect(response).to redirect_to(subjects_path)
      end

      it 'sets flash for record not found' do
        allow(Subject).to receive(:find){ raise ActiveRecord::RecordNotFound}
        delete :destroy, :params => {:id => @subject.id}
        expect(flash[:errors]).to include('Rec not found')
      end
    end

  end
end
