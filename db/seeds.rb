# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

subject = Subject.create(:name => 'A subject', :position => 0, :visible => true)

Page.create(:name => 'A page', :permalink => '123', :position => '0', :visible => true, :subject => subject)
