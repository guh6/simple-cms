class AddSubjectToPages < ActiveRecord::Migration[5.1]
  def change
    add_reference :pages, :subject, foreign_key: true
  end
end
