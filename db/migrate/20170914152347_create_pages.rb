class CreatePages < ActiveRecord::Migration[5.1]
  def change
    create_table :pages do |t|
      t.string :name
      t.integer :permalink, :index => true
      t.integer :position
      t.boolean :visible, :default => false

      t.timestamps
    end
  end
end
