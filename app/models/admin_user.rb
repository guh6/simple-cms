class AdminUser < ApplicationRecord

  has_secure_password
  has_and_belongs_to_many :pages, :join_table => 'admin_users_pages'
  has_many :section_edits

  before_create :set_admin_flag

  default_scope { where(:admin=> true) }
  scope :sorted, lambda{ order(:first_name => :asc).order(:last_name => :asc) }

  scope :last_name_first, lambda{|last, first| order(:last_name => last).order(:first_name => first) }

  self.table_name = 'users'

  def admin_user?
    self.admin
  end

  def self.users_first_and_last_name(sort_order={})
    last_name_sort_order = sort_order.fetch(:last, :asc)
    first_name_sort_order = sort_order.fetch(:first, :asc)
    users = last_name_first(last_name_sort_order, first_name_sort_order)

  end

  def self.users_to_first_and_last(users)
    users.map { |u| "#{u.last_name} #{u.first_name}"}
    rescue []
  end
  
  private
    def set_admin_flag
      self.admin = true
    end

end
