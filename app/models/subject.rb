class Subject < ApplicationRecord
  has_many :pages, :dependent => :destroy

  scope :sorted, lambda{ order(:position=> 'ASC')}
  scope :visible, lambda{ where(:visible => true)}
  scope :search, lambda{ |term| where('name RLIKE ?', "#{term}") }

  validates :name, :presence => true
  validates :visible, :inclusion => {:in => [true, false] }
  validates :position, :presence => true, :numericality => { :greater_than_or_equal_to => 0}
end
