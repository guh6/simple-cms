class AccessController < ApplicationController
  layout 'admin'

  # before_action :confirm_logged_in, :except => [:login, :attempt_login, :logout]
  def menu
    # Display text & links
  end

  def login
    # login form
  end

  def attempt_login
    if params[:email].present? && params[:password].present?
      found_user = AdminUser.where(:email => params[:email]).first
      if(found_user)
        # Returns false or a user object.
        authorized_user = found_user.authenticate(params[:password])
      end
    end

    # Store in session if authorized. Acts as hands-stamp
    if authorized_user
      session[:user_id] = authorized_user.id
      flash[:notice] = 'Successfully Logged in'
      redirect_to(admin_path)
    else
      flash.now[:notice] = 'Invalid username/password combo.'
      render('login')
    end
  end

  def logout
    session[:user_id] = nil
    flash[:notice] = 'Logged out'
    redirect_to access_login_path
  end

  private
    # def confirm_logged_in
    #   unless session[:user_id]
    #     flash[:notice] = "Please login"
    #     redirect_to(access_login_path)
    #   end
    # end
end
