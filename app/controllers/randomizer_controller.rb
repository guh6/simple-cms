require 'faker'
class RandomizerController < ApplicationController
  VALID_TYPES = ['subject', 'page', 'section']
  def show
    if params[:type] and VALID_TYPES.include?(params[:type])
      result = {}
      case params[:type]
      when 'subject' then
        result[:name] = Faker::Movie.quote
        result[:position] = rand(0...50)
        result[:visible] = result[:position].odd?
      when 'page' then
        result[:name] = Faker::Movie.quote
        result[:position] = rand(0...50)
        result[:visible] = result[:position].odd?
      when 'section' then
        result[:name] = Faker::Movie.quote
        result[:position] = rand(0...50)
        result[:visible] = result[:position].odd?
        result[:content] = Faker::Simpsons.quote
        result[:content_type] = 'Simpsons'
      end
      render :json => result.to_json, :status => 200
    else
      render :plain => 'Invalid type specified', :status => 402
    end
  end
end
