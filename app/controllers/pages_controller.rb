class PagesController < ApplicationController

  before_action :confirm_logged_in, :except => [:login, :attempt_login, :logout]
  
  def index
    @pages = Page.where(:subject_id => params[:subject_id])
  end

  def new
    @subject = Subject.find(params[:subject_id])
    @page = Page.new(:subject => @subject)
  end

  def create
    subject = Subject.find(create_params[:subject_id])
    page = subject.pages.new(create_params[:page])
    if page.save
      flash[:notice] = 'Page created'
      redirect_to subjects_path
    else
      flash[:errors] = page.errors.full_messages.join("")
      render :new
    end
  rescue ActiveRecord::RecordNotFound
    flash[:errors] = 'Rec not found'
    redirect_to subjects_path
  end

  def show

  end

  def edit
  end

  def update
  end

  def destroy
    @page = Page.find(params[:id])
    if @page.destroy
      flash[:notice] = 'Destroyed'
      redirect_to subject_path(params[:subject_id])
    else
      flash[:errors] = @page.errors.full_messages
      redirect_to subject_path(params[:subject_id])
    end
  end

  private
    def create_params
      params.permit(:subject_id, :page => [:name, :permalink, :position, :visible])
    end
end
