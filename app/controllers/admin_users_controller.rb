class AdminUsersController < ApplicationController
  layout 'admin'

  PER_PAGE = 10

  before_action :confirm_logged_in, :except => [:login, :attempt_login, :logout]
  before_action :confirm_admin

  def index
    last_name_sort = params.fetch(:last_name, :asc)
    first_name_sort = params.fetch(:first_name, :asc)
    @page_title = 'All Admin Users'
    @admin_users = AdminUser.users_first_and_last_name({:last=> last_name_sort, :first => first_name_sort}).paginate(:page => params[:page], :per_page => PER_PAGE)
    @display_users = AdminUser.users_to_first_and_last(@admin_users)
  end

  def new
  end

  def edit
  end

  def update
  end

  def delete
  end

  def destroy
  end
end
