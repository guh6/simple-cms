class SubjectsController < ApplicationController
  layout 'admin'

  before_action :confirm_logged_in, :except => [:login, :attempt_login, :logout]

  def index
    @subjects = Subject.sorted
    @page_title = 'All Subjects'
  end

  def show
    @subject = Subject.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    render :plain => 'Record not found'
  end

  def new
    @subject = Subject.new(:visible => true)
  end

  def create
    @subject = Subject.create(create_params)
    if(@subject.save)
      flash[:notice] = 'Subject created successfully'
      redirect_to subjects_path
    else
      flash[:error] = @subject.errors
      render :new
    end
  end

  def edit
    @subject = Subject.find(params[:id])
  rescue ActiveRecord::RecordNotFound => e
    render :plain => 'Subject not found!'
  end

  def update
    @subject = Subject.find(params[:id])
    if @subject.update_attributes(create_params)
      flash[:notice] = 'Subject updated successfully'
      redirect_to subject_path(@subject)
    else
      flash[:errors] = @subject.errors
      render :edit
    end
  rescue ActiveRecord::RecordNotFound => e
    flash[:errors] = 'Not found'
    redirect_to subjects_path
  rescue Exception => e
    flash[:errors] = 'PROBLEM WITH UPDATE'
    redirect_to subjects_path
  end

  def delete
    @subject = Subject.find(params[:id])
  rescue ActiveRecord::RecordNotFound => e
    flash[:errors] = 'Record Not Found'
    redirect_to subjects_path
  end

  def destroy
    @subject = Subject.find(params[:id])
    if @subject.destroy
      flash[:notice] = "Subject '#{@subject.name} destroyed successfully"
      redirect_to(subjects_path)
    else
      flash[:errors] = @subject.errors
      render :delete
    end
  rescue ActiveRecord::RecordNotFound => e
    flash[:errors] = 'Rec not found'
    redirect_to(subjects_path)
  rescue Exception => e
    flash[:errors] = e.message
    render :delete
  end

  private
    def create_params
      params.require(:subject).permit(:name, :position, :visible)
    end
end
