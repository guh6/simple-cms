class SectionsController < ApplicationController
  layout 'admin'

  before_action :confirm_logged_in, :except => [:login, :attempt_login, :logout]
  before_action :find_page, :only => [:index, :new, :create]

  def index
    # @page = Page.find(params[:page_id])
    @sections = @page.sections
  end

  def new
    # @page = Page.find(params[:page_id])
    @section = Section.new(:visible => true, :position => 0)
  end

  def create
    # @page = Page.find(create_params[:page_id])
    section = @page.sections.new(create_params[:section])
    if section.save
      flash[:notice] = "Section successfully created!"
      redirect_to page_sections_path(@page)
    else
      flash[:errors] = section.errors.full_messages
      render :new
    end
  rescue Exception => e
    flash[:errors] = e.message
    redirect_to page_sections_path(@page)
  end

  def destroy
    section = Section.find(params[:id])
    if(section.destroy)
      flash[:notice] = 'Section succesfully destroyed!'
      redirect_to page_sections_path(section.page.id)
    else
      flash[:errors] = section.errors.full_messages
      redirect_to page_sections_path(section.page.id)
    end
  rescue ActiveRecord::RecordNotFound => e
    flash[:errors] = 'Cannot find this section'
    redirect_to :back
  rescue Exception => e
    flash[:errors] = e.message
    redirect_to page_sections_path(section.page.id)
  end

  private
    def create_params
      params.permit(:page_id, :section => [:name, :position, :visible, :content, :content_type])
    end

    def find_page
      @page = Page.find(params[:page_id])
    end
end
