class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  private
    def confirm_logged_in
      unless session[:user_id]
        flash[:notice] = "Please login"
        redirect_to(access_login_path)
      end
    end

    def confirm_admin
      admin_user = AdminUser.find(session[:user_id]) rescue nil
      unless admin_user
        flash[:notice] = 'Not an Admin!'
        redirect_to(access_login_path)
      end
    end
end
