module PagesHelper

  def show_sections(page)
    sections = page.sections.count || 0
    if sections > 0
      link_to("Show Sections(#{sections})", page_sections_path(:page_id => page.id))
    else
      ''
    end
  rescue Exception => e
    ''
  end

end
