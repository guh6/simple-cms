# Simple CMS Blueprint
Left navigation Group by Subject
Content of each page is displayed on the right
Each page can be composed of several sections

Subject (1) -> (M) Page (1)  -> (M)Section
- A subject has many pages. A page belongs to a subject
- A page has many sections. A section belongs to a page.

## Models:
All models require indexes.

### Subject
  - name(string)
  - position(int)
  - visible(boolean)  

### Page
  - subject_id(integer)
  - name(string)
  - permalink(integer)
  - position(integer)
  - visible(boolean)

### Section
  - page_id(integer)
  - name (string)
  - position(integer)
  - visible(boolean)
  - content_type(string)
  - content(text)

### User
  - first_name(string)
  - last_name(string)
  - email(string)
  - password(string)
  - admin(boolean)

### AdminUser
 Inherits from User table. Difference is admin = true.

### SectionEdit
  This is a rich join table on AdminUser and Section models.

## Controllers

### SubjectsController

### SectionsController

### AdminUsersController
