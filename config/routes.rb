Rails.application.routes.draw do

  resources :admin_users, :except => [:show]


  get  'admin', :to => 'access#menu' # Redirect to login form if success
  get  'access/menu'
  get  'access/login'
  post 'access/attempt_login'
  get  'access/logout'


  # For the randomizer controller
  get 'randomizer/show'

  # For the subjects resource that has child pages
  resources :subjects do
    member do
      get :delete
    end

    # Each page requires a parent page to be created and viewed
    resources :pages, only: [:index, :new, :create]
  end
  resources :pages, only: [:show, :edit, :update, :destroy]

  # For the pages resource that has child sections
  resources :pages do
    resources :sections, :only => [:index, :new, :create]
  end

  resources :sections, :only => [:show, :edit, :update, :destroy]

  #  Demo controllers
  get 'tests/index'
  get 'tests/welcome'
  get 'tests/escape_output'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/cms', :to => 'subjects#index'
  get '/', :to => redirect('/static_page.html')
end
